#
# spec file for package urbackup-server
#
# Copyright (c) 2017 Alexander Rebrov <ralexn@gmail.com>
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.
# Please submit bugfixes or comments via http://bugs.opensuse.org/
#

Name: urbackup-client
Version: 2.4.11
Release: 2
Summary: Efficient Client-Server backup system for Linux and Windows

%if 0%{?suse_version}
Group: Productivity/Archiving/Backup
%endif

License: AGPL-3.0+
URL: http://www.urbackup.org/
Source0: %{name}-%{version}.tar.gz

%if 0%{?suse_version}
Source2: %{name}.rpmlintrc
PreReq: %fillup_prereq permissions
%endif


BuildRequires: gcc, gcc-c++
BuildRequires: libcurl-devel
BuildRequires: fuse-devel
BuildRequires: zlib-devel
%if 0%{?suse_version}
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: pkg-config
BuildRequires: libtool
%endif

Requires: systemd


%if 0%{?suse_version}
Requires: libcurl4 >= 7.20.0
Requires: libz1
Requires: guestfs-tools
%endif

%if 0%{?fedora}%{?el7}
Requires: libcurl  >= 7.20.0
Requires: zlib
Requires: libguestfs-tools
%endif

%if 0%{?fedora} >= 17
Requires(post): /usr/bin/chown
Requires(pre): /usr/bin/mkdir
Requires(pre): /usr/bin/sed 
%else
Requires(post): /bin/chown
Requires(pre): /bin/mkdir
Requires(pre): /bin/sed 
%endif

%if 0%{?suse_version}
Requires(pre): pwdutils
%else
Requires(pre): /usr/sbin/useradd
Requires(pre): /usr/sbin/groupadd
%endif
Requires(post): systemd
Requires(post): /sbin/ldconfig
Requires(preun): systemd
Requires(postun): /sbin/ldconfig

# Compat macro for new _fillupdir macro introduced in Nov 2017
%if ! %{defined _fillupdir}
%define _fillupdir /var/adm/fillup-templates
%endif

%description
Efficient Client-Server Backup system for Linux and Windows
with GPT and UEFI partition. A client for Windows lets you 
backup open files and complete partition images. Backups 
are stored to disks in a efficient way (deduplication) 
on either Windows or Linux servers.

%prep
%setup -q -n %{name}-%{version}

%build
cd %{name}-%{version}.0

export SUID_CFLAGS=-fPIE
export SUID_LDFLAGS=-fpie
%ifarch %{ix86}
export CXXFLAGS="-msse2 -O2 -g"
%endif
%ifarch aarch64
export CXXFLAGS="-O2 -g -march=armv8-a+crc"
%endif
%configure --enable-packaging  --with-mountvhd --enable-embedded-cryptopp --enable-headless --enable-embedded-zstd
make %{?_smp_mflags}


%install
cd %{name}-%{version}.0

%make_install
mkdir -p %{buildroot}%{_unitdir}
mkdir -p %{buildroot}%{_mandir}/man1
mkdir -p %{buildroot}%{_localstatedir}/log/
mkdir -p %{buildroot}%{_localstatedir}/urbackup
%if 0%{?suse_version}
mkdir -p %{buildroot}%{_fillupdir}
mkdir -p %{buildroot}%{_bindir}
ln -s /usr/sbin/service %{buildroot}%{_bindir}/rc%{name}
%endif
%if 0%{?fedora}%{?el7}
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig
mkdir -p %{buildroot}%{_initrddir}
mkdir -p %{buildroot}%{_prefix}/lib/firewalld/services
%endif

%if 0%{?suse_version}
install -m 644 defaults_server %{buildroot}%{_fillupdir}/sysconfig.%{name}
%endif
%if 0%{?fedora}%{?el7}
install -m 644 %{_builddir}/%{name}-%{version}.0/defaults_server %{buildroot}%{_sysconfdir}/sysconfig/%{name}
install -m 640 %{_builddir}/%{name}-%{version}.0/urbackup-server-firewalld.xml %{buildroot}%{_prefix}/lib/firewalld/services/%{name}.xml
%endif
install -m 644 %{_builddir}/%{name}-%{version}.0/docs/urbackupclientbackend.1 %{buildroot}%{_mandir}/man1/%{name}.1

install -m 755 %{_builddir}/urbackupclientbackend-meego.service %{buildroot}%{_unitdir}/%{name}.service


%pre
getent group urbackup > /dev/null || groupadd -r urbackup
:
%if 0%{?suse_version}
getent passwd urbackup > /dev/null || useradd -r -g urbackup  \
 -s /sbin/nologin urbackup >/dev/null 2>&1 
:
%service_add_pre %{name}.service
%endif
%if 0%{?fedora}%{?el7}
getent passwd urbackup > /dev/null || useradd -r -g urbackup  \
 -s /sbin/nologin urbackup >/dev/null 2>&1
:
%endif

%post
%if 0%{?suse_version}
/sbin/ldconfig
%fillup_only -n %{name}
%service_add_post %{name}.service
%endif
%if 0%{?fedora}%{?el7}
%systemd_post %{name}.service
%endif

%preun
%if 0%{?suse_version}
%service_del_preun %{name}.service
%endif
%if 0%{?fedora}%{?el7}
%systemd_preun %{name}.service
%endif

%postun
%if 0%{?suse_version}
%service_del_postun %{name}.service
%endif
%if 0%{?fedora}%{?el7}
%systemd_postun_with_restart %{name}.service
%endif
/sbin/ldconfig


%posttrans
%if 0%{?have_systemd}
if systemctl --quiet is-enabled %{name}.service 2>/dev/null
then 
   /bin/systemctl --quiet --force reenable %{name} 2>/dev/null || :
else 
   /bin/systemctl --quiet --force reenable %{name} 2>/dev/null || :
   /bin/systemctl --quiet disable %{name} 2>/dev/null || :
fi
%endif


%files
%defattr(-,root,root)
%doc %{name}-%{version}.0/AUTHORS %{name}-%{version}.0/COPYING %{name}-%{version}.0/ChangeLog %{name}-%{version}.0/README
%dir %attr(0755,urbackup,urbackup) %{_datadir}/urbackup
%attr(-,urbackup,urbackup) %{_datadir}/urbackup/*
%{_bindir}/*
%{_sbindir}/*
%{_mandir}/man1/*
%if 0%{?suse_version}
%config %{_fillupdir}/sysconfig.%{name}
%endif
%if 0%{?fedora}%{?el7}
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}
%{_prefix}/lib/firewalld/services/%{name}.xml
%endif
%{_localstatedir}/log/
%dir %attr(0755,urbackup,urbackup) %{_localstatedir}/urbackup
%{_localstatedir}/urbackup/*
%{_sysconfdir}/urbackup/*
%attr(0644,root,root) %{_unitdir}/%{name}.service


%changelog
*Tue Mar 07 2017 Alexander Rebrov <ralexn@gmail.com> 2.1.19.1765
- Add urbackup_mount_helper to files section

*Fri Feb 17 2017 Alexander Rebrov <ralexn@gmail.com> 2.1.18.1750
- Add new version 2.1.18.1750
- Delete /sbin directory from install scripts

*Mon Jan 30 2017 Alexander Rebrov <ralexn@gmail.com> 2.0.38.1660-41.1
- Create /var/urbackup folder with install and files macro
- Edit Requires (libcurl, zlib different name packages of SUSE and EL)
- Edit Requires(pre). Add some utils.
- Change license to AGPL-3.0+
- Delete error from urbackup-server.service in SUSE distributions in sed command

*Tue Jan 17 2017 Alexander Rebrov <ralexn@gmail.com> 2.0.38.1660
- Add SuSE Linux distributions for OBS

*Wed Jun 29 2016 Alexander Rebrov <ralexn@gmail.com> 2.0.29 RC
- Update to version 2.0.29 RC

*Wed Jun 01 2016 Alexander Rebrov <ralexn@gmail.com> 2.0.24 beta
- Update to version 2.0.24-0.0 beta

*Thu May 26 2016 Alexander Rebrov <ralexn@gmail.com> 2.0.22 beta
- Update to version 2.0.22-0.0 beta

*Thu May 19 2016 Alexander Rebrov <ralexn@gmail.com> 2.0.21 beta
- Update to version 2.0.21-0.0 beta

*Wed May 11 2016 Alexander Rebrov <ralexn@gmail.com> 2.0.17 beta
- Update to version 2.0.17-0.0 beta

*Wed May 04 2016 Alexander Rebrov <ralexn@gmail.com> 2.0.14 beta
- Update to version 2.0.14-0.0 beta

*Fri Apr 29 2016 Alexander Rebrov <ralexn@gmail.com> 2.0.13 beta
- Update to version 2.0.13-0.0 beta

*Fri Apr 22 2016 Alexander Rebrov <ralexn@gmail.com> 2.0.12 beta
- Update to version 2.0.12-0.0 beta

*Tue Apr 19 2016 Alexander Rebrov <ralexn@gmail.com> 2.0.11 beta
- Update to version 2.0.11-0.0 beta, edit BuildRequires and Requires

*Thu Apr 14 2016 Alexander Rebrov <ralexn@gmail.com> 2.0.10 beta
- Adapted spec to version 2.0.10-0.0 beta from version 1.4.14

