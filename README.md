```
tar -xvf urbackup-client-*.tar.gz
mb2 -X -t SailfishOS-4.4-aarch64 build -p
```

When switching between SailfishOS targets, clean the urbackup-client directory; either `make clean` or remove it altogether.
